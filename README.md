
## Project setup

1. Replace `app-name` with your real app name

1. Install NodeJS (above the 10.17 version is recommended, edit .nvmrc and package.json if yoy want to use different version)
https://nodejs.org/en/download/package-manager/
or via NVM

1. Install PostgreSQL https://www.postgresql.org/download/
and create `app-name` database

1. Open terminal in a current directory and put commands here
    ```bash
    npm install -g yarn
    npm install -g yeoman
    
    yarn install
    yarn migrate
    ```

1. Now you are able to start development with `yarn dev`


## Development

### Code verification
Choose your NodeJS version and enable ESlint and Prettier support in your IDE.

### Generators
You can use generators to speed-up development.

#### Scaffold generation
Most efficient. Will create model, migration, routes and swagger annotations.
```bash
yo ./_generate.js User --attributes name:string email:string
yo ./_generate.js User --attributes "name:string(50)"
yo ./_generate.js User --attributes "names:array(string)"
yo ./_generate.js User --attributes "names:[string]"
```
All supported data types you can found at https://sequelize.org/v5/manual/data-types.html
Edit `./_templates/*.js.ejs` files in case you will need to make changes in the templates.\

#### Model generation
```bash
npx sequelize model:create --name User --attributes name:string,email:string
```

#### Migration generation
```bash
npx sequelize migration:generate --name add-password-to-user
```

run `npx sequelize --help` to see more commands


## Available Scripts

### Development

#### `yarn install`
Will setup all dependencies for front-end and back-enf

#### `yarn migrate`
Apply migration locally

#### `yarn dev`
Runs the app in the development mode.\
The api will reload if you make edits.


### Production

#### `yarn prod`
Start or restart production process using pm2

#### `yarn prod:start`
First time starting production process using pm2

#### `yarn prod:restart`
Restart production process using pm2

#### `yarn prod:delete`
Kill production process using pm2 

#### `yarn prod:migrate`
Apply migration locally on production


### Code quality

#### `yarn lint`
Verify code style
    
#### `yarn lint:fix`
Verify code style with autofixing.\
Also will be runned automatically on every commit.

